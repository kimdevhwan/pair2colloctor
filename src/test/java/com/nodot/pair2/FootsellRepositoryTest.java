package com.nodot.pair2;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.background.pair2.Pair2collectorApplication;
import com.nodot.background.pair2.repository.collector.FootsellRepository;
import com.nodot.background.pair2.vo.collector.FootsellVO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {Pair2collectorApplication.class})
public class FootsellRepositoryTest {
	
	@Autowired
	FootsellRepository footsellRepository;
	
	@Test
	public void customSaveAllTest() {
		FootsellVO test = new FootsellVO();
		test.setUSER_NICK("test");
		test.setSTOCK_NAME("조던11 콩코드");
		test.setSTOCK_SIZE("270");
		test.setSTOCK_URL("adsfa");
		test.setREG_DATE("2018-12-01");
		
		List<FootsellVO> tests = new ArrayList<>();
		tests.add(test);
		footsellRepository.saveAll(tests);
		
	}

}
