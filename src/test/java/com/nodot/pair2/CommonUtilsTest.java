package com.nodot.pair2;

import static org.junit.Assert.assertTrue;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.background.pair2.common.CollectorUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {CollectorUtils.class})
public class CommonUtilsTest {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CollectorUtils utils;
	
	@Test
	public void castPriceWonTest() {
		String[] rawPrice = {
				"ㆍ희망가격 :     109.9",
				"ㆍ희망가격 :     1.099.000",
				"ㆍ희망가격 :     택포109.9만원",
				"ㆍ희망가격 :     280,000만원",
				"ㆍ희망가격 :     280",
				"ㆍ희망가격 :     28.9만원",
				"ㆍ희망가격 :     99,000원",
				"ㆍ희망가격 :     99.000원",
				"ㆍ희망가격 :     9.9",
				"ㆍ희망가격 :     75,9",
				"ㆍ희망가격 : 택배비포함. 50",
				"ㆍ희망가격 : 택배시, 125만원"
		};
		
		Arrays.asList(rawPrice).stream().forEach(price -> {
			logger.info("{}, {}", price, utils.castPriceWon(price));
		});
	}
	
	@Test
	public void isSearchDateRangeTest() {
		assertTrue(utils.isSearchDateRange("20181110", "20181101", "20181112"));
	}
	
	@Test
	public void getLastDateTest() {
		logger.info("naver searching date format : {}",utils.getLastDate("yyyy-MM-ddyyyy-MM-dd"));
		logger.info(LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-ddyyyy-MM-dd")));
	}
}
