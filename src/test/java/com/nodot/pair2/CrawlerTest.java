package com.nodot.pair2;

import java.io.IOException;
import java.util.regex.Matcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrawlerTest {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String footsellBaseUrl = "https://footsell.com/g2/bbs/board.php?bo_table=m51&product_status=1&page=";
	
	private String tagStock = "div#list_table > div.list_table_row";
	private String tagUserNick = "div.list_market_name";
	private String tagSize = "span.list_market_size";
	private String tagPrice = "div.list_market_price";
	private String tagName = "a.list_subject_a";
	private String tagCrawlingDate = "span.list_table_dates";
	private String tabIsNew = "span.list_market_used";
	private String tagSizeWonUnit = "span.list_market_size_mm";
	private String tagViewCount = "div.float_right.list_table_col_hit";
	
	private String regexBlindFilter = ".*((?:블라인드)|(?:교환)|(?:일괄)|(?:(?i)ps)|(?:(?i)bp)|(?:(?i)td)).*";
	
	@Test
	public void crawlingFootsellTest() {
		try {
			//+"&sfl=wr_subject&sop=and&stx=gs"
			Document doc = Jsoup.connect(footsellBaseUrl+"1").get();
			
			Elements stockList = doc.select(tagStock);
			stockList.select(tagSizeWonUnit).remove();
			
			stockList.stream()
				.filter(f -> f.select(tagSize).text().length() < 4)	//여러 사이즈 일괄 등록 필터링
				.filter(f -> f.select(tagPrice).text().length() > 5) //1만원 이하 가격 필터링
				.filter(f -> !f.select(tagName).text().matches(regexBlindFilter))
				.forEach(stock -> {
				logger.info("nick  : {}", stock.select(tagUserNick).text());
				logger.info("name  : {}", stock.select(tagName).text());
				logger.info("size  : {}", stock.select(tagSize).text());
				logger.info("price : {}", Integer.parseInt(stock.select(tagPrice).text().replaceAll(",", "")));
				logger.info("url   : {}", stock.select(tagName).attr("abs:href"));
				logger.info("date  : {}", stock.select(tagCrawlingDate).text());
				logger.info("new   : {}", stock.select(tabIsNew).text().equals("새제품") ? true : false);
				logger.info("soldout   : {}", stock.select(tagUserNick).text().equals("거래완료") ? true : false);
				logger.info("viewcount   : {}", stock.select(tagViewCount).text());
			});
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void crawlingNinkemaniaTest() {
		//url
		String nikemaniaNCafeSearchUrl = "https://cafe.naver.com/ArticleSearchList.nhn?search.clubid=10625158&search.menuid=369&search.boardtype=L&userDisplay=50&search.page=1";
		
		//html 태그
		String tagStocksArea = "form[name=ArticleList] > table.board-box > tbody > tr[align=center]";
		String tagRegDate = "td.view-count.m-tcol-c";
		String tagSellingStatus = "span.head";
		String tagStockTitle = "span.aaa > a.m-tcol-c";
		String tagStockUrl = "abs:href";
		String tagSellerNick = "div.pers_nick_area > table > tbody > tr > td.p-nick > a";
		String tagViewCount = "td.view-count.m-tcol-c";
		String tagStockDetaillArea = "div.inbox";
		String tagStockDetailRegDate = "div.tit-box > div.fr";
		String tagStockDetail = "#tbody > div";
		
		//수동 로그인
		
		
		//파싱
		Document doc = null;
		try {
			doc = Jsoup.connect(nikemaniaNCafeSearchUrl).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements stockList = doc.select(tagStocksArea);
		stockList.stream()
		.filter(c -> !c.select(tagStockTitle).text().matches(".+[,|\\/|\\&|\\|].+"))	//다중판매 필터링
		.filter(c -> !c.select(tagStockTitle).text().matches(regexBlindFilter))	//다중판매 필터링
		.forEach( stock -> {
			
			
			logger.info("url   : {}", stock.select(tagStockTitle).attr(tagStockUrl));
			logger.info("soldout   : {}", stock.select(tagSellingStatus).text().equals("판매완료") ? true : false);
			logger.info("viewcount   : {}", stock.select(tagViewCount).get(1).text());
		});
	}

}
