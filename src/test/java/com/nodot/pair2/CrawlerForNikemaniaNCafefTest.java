package com.nodot.pair2;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Optional;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.yaml.snakeyaml.util.UriEncoder;

import com.nodot.background.pair2.common.NaverCafeUtils;

@SpringBootTest
public class CrawlerForNikemaniaNCafefTest {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private boolean optinalResult = false;

	@Test
	public void optionalCookiesTest() {
		
		NaverCafeUtils utils = new NaverCafeUtils();
		
		Optional.ofNullable(utils.getCookie()).ifPresent(cookies -> {
			optinalResult = true;
		});
		
		assertFalse(optinalResult);
		
		utils.setCookies(new HashMap<String, String>());
		
		Optional.ofNullable(utils.getCookie()).ifPresent(cookies -> {
			optinalResult = true;
		});
		
		assertTrue(optinalResult);
	}
	
	@Test
	public void koreanEncodingTest() {
		String name = "이지+350+지브라";
		try {
			logger.info("{}", URLEncoder.encode(name, "EUC-KR"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
