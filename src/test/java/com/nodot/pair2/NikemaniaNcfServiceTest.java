package com.nodot.pair2;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.background.pair2.Pair2CommandLineRunner;
import com.nodot.background.pair2.Pair2collectorApplication;
import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.NaverCafeUtils;
import com.nodot.background.pair2.repository.collector.NikemaniaNcfRepository;
import com.nodot.background.pair2.service.CrawlerForNikemaniaNcf;
import com.nodot.background.pair2.service.NikemaniaNcfService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Pair2collectorApplication.class, Pair2CommandLineRunner.class})
public class NikemaniaNcfServiceTest {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	NaverCafeUtils naverUtils;
	
	@Autowired
	NikemaniaNcfService nikemaniaNcfService;
	
	/**
	 * 수동 로그인이기 때문에 테스트로 적합치 않음
	 */
	@Test
	public void createCookiesTest() {
		try {
			nikemaniaNcfService.createCookies();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
