package com.nodot.pair2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import javax.print.attribute.HashAttributeSet;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import com.gargoylesoftware.htmlunit.WebClient;
import com.nodot.background.pair2.Pair2collectorApplication;
import com.nodot.background.pair2.common.NaverCafeUtils;

@SpringBootTest(classes= {Pair2collectorApplication.class})
public class NaverCafeTest {
	
	private static Logger logger = LoggerFactory.getLogger("Unit Test");
	
	@Test
	public void login() {
		String id = "anchjdh";
		String pw = "seyeon042305";
		NaverCafeUtils naver = new NaverCafeUtils();
		WebClient webClient = naver.initWebClient();
		try {
			webClient = naver.login(id,pw,webClient);
			assertTrue(naver.isLogined(webClient));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void loginNout() {
		String id = "anchjdh";
		String pw = "seyeon042305";
		NaverCafeUtils naver = new NaverCafeUtils();
		WebClient webClient = naver.initWebClient();
		try {
			webClient = naver.login(id,pw,webClient);
			assertTrue(naver.isLogined(webClient));
			
			webClient = naver.logout(webClient);
			assertFalse(naver.isLogined(webClient));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void reLogin() {
		String id = "anchjd";
		String pw = "seyeon042305";
		NaverCafeUtils naver = new NaverCafeUtils();
		WebClient webClient = naver.initWebClient();
		
		long waitTimeMillis = 1000;
		int tryLoginCount = 0;
		
		try {
			webClient = naver.login(id, pw, webClient);
			while(!naver.isLogined(webClient)) {
				waitTimeMillis += 1000;
				tryLoginCount++;
				logger.info("======== try to relogin : {} =======", tryLoginCount);
				logger.info("======== modified wait time millis {} =======", waitTimeMillis);
				
				if(tryLoginCount == 5) {
					id = "anchjdh";
				} else if(tryLoginCount > 5) {
					break;
				}
				
				webClient = naver.initWebClient(waitTimeMillis);
				webClient = naver.login(id, pw, webClient);
			}
			assertTrue(naver.isLogined(webClient));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getCookies() {
		String id = "anchjdh";
		String pw = "seyeon042305";
		NaverCafeUtils naver = new NaverCafeUtils();
		WebClient webClient = naver.initWebClient();
		Map<String,String> cookies = null;
		try {
			webClient = naver.login(id,pw,webClient);
			
			boolean isLgined = naver.isLogined(webClient);
			assertTrue(isLgined);

			cookies = naver.getCookie(webClient);
			assertNotNull(cookies);
			assertTrue(cookies.keySet().contains("DA_HC"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
