package com.nodot.pair2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.background.pair2.Pair2CommandLineRunner;
import com.nodot.background.pair2.Pair2collectorApplication;
import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.summary.NikemainiaNcfSummary;
import com.nodot.background.pair2.repository.collector.FootsellRepository;
import com.nodot.background.pair2.repository.collector.NikemaniaNcfRepository;
import com.nodot.background.pair2.vo.collector.ExternalMarketVO;
import com.nodot.background.pair2.vo.collector.FootsellVO;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;
import com.nodot.background.pair2.vo.summary.ProductVO;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Pair2collectorApplication.class, Pair2CommandLineRunner.class})
@Log4j2
public class SummaryBugTest {
	
	@Autowired
	NikemaniaNcfRepository nikemaniaNcfRepository;
	
	@Autowired
	FootsellRepository footsellRepository;
	
	@Autowired
	NikemainiaNcfSummary summary;
	
	@Autowired
	private NikemainiaNcfSummary nikemaniaNcfSummary;
	
	@Autowired
	private CollectorUtils collectorUtils;
	
	@Test
	public void bugTest() {
		
		String startDate = "2018-11-01 00:00:00";
		String endDate = "2018-11-30 23:59:59";
		
		List<ExternalMarketVO> classifiedStocks =  new ArrayList<ExternalMarketVO>();
		List<NikemaniaNcfVO> nikemaniaNcfStocks = nikemaniaNcfRepository.findSummarizeDataByDate(startDate, endDate);
		List<FootsellVO> footsellStocks = footsellRepository.findSummarizeDataByDate(startDate, endDate);
		
		for(ProductVO product : summary.allProductList()) {
			classifiedStocks.addAll(
				nikemaniaNcfStocks.stream().parallel()
					.filter(f -> f.getSTOCK_NAME().matches(product.getClassifyRegex()))
					.filter(f -> this.filtering(f.getSTOCK_NAME(), product.getFilterRegex())).collect(Collectors.toList())
	//				.forEach(shoes -> {
	//					log.info("등록되 제품 필터링 : {}->{}",shoes.getUSER_NICK(), shoes.getSTOCK_NAME());
	//				});
			);
			
			classifiedStocks.addAll(
				footsellStocks.stream().parallel()
				.filter(f -> f.getSTOCK_NAME().matches(product.getClassifyRegex()))
				.filter(f -> this.filtering(f.getSTOCK_NAME(), product.getFilterRegex())).collect(Collectors.toList())
			);
			
			
		}
		
		log.info("filtered : {}",classifiedStocks.size());
		log.info("nmania : {}",nikemaniaNcfStocks.size());
		log.info("footsell : {}",footsellStocks.size());
		
//		nikemaniaNcfSummary.saveSummary(collectorUtils.sortData(classifiedStocks));
		
	}
	
	public boolean filtering(String name, String regex) {
		if(regex == null) {
			log.info("필터링 조건 없음");
			return true;
		}
		
		if(name.matches(regex)) {
			log.info("필터링  : {}, {}",name, regex);
			return false;
		} else {
			log.info("필터링 대상 아님  : {}, {}",name, regex);
			return true;
		}
	}
	
	@Test
	public void regexTest() {
		String regex = ".*((?:골드)|(?:로우)|(?:(?i)gs)|(?:(?i)bg)|(?:(?i)bp)).*";
		String name = "조던 11 콩코드 로우";
		
		if(name.matches(regex)) {
			log.info("되?");
		} else {
			log.info("안되?");
		}
	}
	
	
	
}
