package com.nodot.background.pair2.repository.summary;

import org.springframework.data.repository.CrudRepository;
import com.nodot.background.pair2.vo.summary.SummaryVO;

public interface SummaryRepository extends CrudRepository<SummaryVO, Integer> {
	
}
