package com.nodot.background.pair2.repository.collector;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nodot.background.pair2.vo.collector.FootsellVO;

public interface FootsellRepository extends JpaRepository<FootsellVO, Integer> {
	
	@Query(value = "select *\r\n" + 
			"from\r\n" + 
			"    DATA_FOOTSELL\r\n" + 
			"where\r\n" + 
			"        REG_DATE BETWEEN :start and :end\r\n" + 
			"        and IS_NEW = 1\r\n" + 
			"        and IS_SUM = 0\r\n" + 
			"group by USER_NICK , STOCK_NAME , STOCK_SIZE , STOCK_PRICE , REG_DATE\r\n" + 
			"order by REG_DATE", nativeQuery = true)
	List<FootsellVO> findSummarizeDataByDate(@Param("start") String startDate, @Param("end") String endDate);
	
	@Query(value = "select *\r\n" + 
			"from\r\n" + 
			"    DATA_FOOTSELL\r\n" + 
			"where\r\n" + 
			"    STOCK_NAME LIKE :item\r\n" + 
			"        and REG_DATE BETWEEN :start and :end\r\n" + 
			"        and IS_NEW = 1\r\n" + 
			"        and IS_SUM = 0\r\n" + 
			"group by USER_NICK , STOCK_NAME , STOCK_SIZE , STOCK_PRICE , REG_DATE\r\n" + 
			"order by REG_DATE", nativeQuery = true)
	List<FootsellVO> findSummarizeDataBySearchItem(@Param("start") String startDate, @Param("end") String endDate, @Param("item") String searchItem);

}