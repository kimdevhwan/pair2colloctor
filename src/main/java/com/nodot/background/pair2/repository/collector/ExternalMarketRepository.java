package com.nodot.background.pair2.repository.collector;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.nodot.background.pair2.vo.collector.ExternalMarketVO;

@NoRepositoryBean
public interface ExternalMarketRepository<T, ID extends Serializable> extends CrudRepository<T, ID> {
}
