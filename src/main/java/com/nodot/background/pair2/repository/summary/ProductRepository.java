package com.nodot.background.pair2.repository.summary;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nodot.background.pair2.vo.summary.ProductVO;

public interface ProductRepository extends JpaRepository<ProductVO, Integer> {
	
}
