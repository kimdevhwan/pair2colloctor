package com.nodot.background.pair2.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = {
				"com.nodot.background.pair2.repository.summary",
				"com.nodot.background.pair2.vo.summary" },
		entityManagerFactoryRef = "summaryEntityManagerFactory",
		transactionManagerRef = "summaryTransactionManager")
public class SummaryDataSource {
	
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource.summary")
	public DataSource summaryDatasource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean summaryEntityManagerFactory(
			EntityManagerFactoryBuilder builder, @Qualifier("summaryDatasource") DataSource summaryDatasource) {
		return builder.dataSource(summaryDatasource)
				.packages("com.nodot.background.pair2.repository.summary",
						"com.nodot.background.pair2.vo.summary")
				.build();
	}
	
	@Bean
	public PlatformTransactionManager summaryTransactionManager(
			@Qualifier("summaryEntityManagerFactory") EntityManagerFactory summaryEntityManagerFactory) {
		return new JpaTransactionManager(summaryEntityManagerFactory);
	}

}
