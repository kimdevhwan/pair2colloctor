package com.nodot.background.pair2.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = {
				"com.nodot.background.pair2.repository.collector",
				"com.nodot.background.pair2.vo.collector" },
		entityManagerFactoryRef = "collectorEntityManagerFactory",
		transactionManagerRef = "collectorTransactionManager")
public class CollectorDataSource {
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource.collector")
	public DataSource collectorDatasource() {
		return DataSourceBuilder.create().build();
	}
	
	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean collectorEntityManagerFactory(
			EntityManagerFactoryBuilder builder, @Qualifier("collectorDatasource") DataSource collectorDatasource) {
		return builder.dataSource(collectorDatasource)
				.packages("com.nodot.background.pair2.repository.collector",
						"com.nodot.background.pair2.vo.collector")
				.build();
	}
	
	@Primary
	@Bean
	public PlatformTransactionManager collectorTransactionManager(
			@Qualifier("collectorEntityManagerFactory") EntityManagerFactory collectorEntityManagerFactory) {
		return new JpaTransactionManager(collectorEntityManagerFactory);
	}

}
