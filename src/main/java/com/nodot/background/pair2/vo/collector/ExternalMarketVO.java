package com.nodot.background.pair2.vo.collector;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.repository.NoRepositoryBean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoRepositoryBean
public abstract class ExternalMarketVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IDX;
	private String USER_NICK;
	private String STOCK_NAME;
	private String STOCK_SIZE;
	private int STOCK_PRICE;
	private int VIEW_COUNT;
	private String STOCK_URL;
	private String REG_DATE;
	private boolean IS_NEW = false;
	private boolean IS_SUM = false;

}
