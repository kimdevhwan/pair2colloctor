package com.nodot.background.pair2.vo.summary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "MARKET_TREND", schema = "PAIR2")
public class SummaryVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IDX;
	private String SELLER_NICK;
	private String REG_DATE;
	private String SIZE;
	private int PRICE;
	private int VIEW_COUNT;
	private boolean IS_NEW = true;
	private String DATA_SOURCE;
	private int	EXTERNAL_IDX;
	private int PRODUCT_IDX;

}
