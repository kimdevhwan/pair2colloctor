package com.nodot.background.pair2.vo.summary;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "PRODUCT", schema = "PAIR2")
public class ProductVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;
	
	@Column(name = "CLASSIFY_REGEX", nullable = false)
	private String classifyRegex;
	
	@Column(name = "FILTER_REGEX", nullable = true)
	private String filterRegex;
	
}
