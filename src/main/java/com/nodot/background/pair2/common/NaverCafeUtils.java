package com.nodot.background.pair2.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

@Component
public class NaverCafeUtils {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String sslLoginUrl = "https://nid.naver.com/nidlogin.login";
	private final String sslLogoutUrl = "https://nid.naver.com/nidlogin.logout";
	
	private final String idXPath = "//*[@id=\"id\"]";
	private final String pwXpath = "//*[@id=\"pw\"]";
	private final String loginBtnXPath = "//*[@id=\"frmNIDLogin\"]/fieldset/input";
	
	private Map<String, String> cookies;
	
	public WebClient initWebClient() {
		WebClient webClient = new WebClient();
		webClient = new WebClient(BrowserVersion.CHROME);
        webClient.waitForBackgroundJavaScript(1000);
        webClient.setJavaScriptTimeout(1000);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        return webClient;
	}
	
	public WebClient initWebClient(long waitTimeMillis) {
		WebClient webClient = new WebClient();
		webClient = new WebClient(BrowserVersion.CHROME);
        webClient.waitForBackgroundJavaScript(waitTimeMillis);
        webClient.setJavaScriptTimeout(waitTimeMillis);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        return webClient;
	} 
	
	public WebClient login(String ID, String PW, WebClient webClient) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		HtmlPage currPage = webClient.getPage(sslLoginUrl);
		HtmlTextInput inputId = currPage.getFirstByXPath(idXPath);
		HtmlPasswordInput inputPw = currPage.getFirstByXPath(pwXpath);
		HtmlSubmitInput loginBtn = currPage.getFirstByXPath(loginBtnXPath);
		
		inputId.setValueAttribute(ID);
		inputPw.setValueAttribute(PW);
		loginBtn.click();
		
		return webClient;
	}
	
	public WebClient logout(WebClient webClient) throws Exception {
		logger.info("====================================== Logout processing ======================================");
		webClient.getPage(sslLogoutUrl);
		return webClient;
	}
	
	public boolean isLogined(WebClient webClient) {
		HtmlPage currPage = (HtmlPage) webClient.getCurrentWindow().getEnclosedPage();
		return !currPage.asText().contains("Naver Sign in");
	}
	
	public Map<String, String> getCookie(WebClient webClient) {
		webClient.getCookieManager().getCookies().forEach(c -> {
			cookies.put(c.getName(), c.getValue());
		});
		return cookies;
	}
	
	/**
	 * 자동 입력 방지로 자동 로그인 불가하여 수동 로그인 후 쿠키값 얻기
	 * @throws InterruptedException 
	 */
	public void createCookiesByBrowser() throws InterruptedException {
		cookies = new HashMap<String, String>();
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\kimdehwan\\Desktop\\selenuim dirver\\chromedriver_win32\\chromedriver.exe");
		
		WebDriver chrome = new ChromeDriver();
		chrome.get(sslLoginUrl);	//수동 로그인을 위한 브라우저 로딩
		
		Thread.sleep(7000);	//로그인 정보 입력까지 대기
		
		chrome.manage().getCookies().forEach(c -> {
			cookies.put(c.getName(), c.getValue());
		});
	}
	
	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}
	
	public Map<String, String> getCookie() {
		return cookies;
	}

}
