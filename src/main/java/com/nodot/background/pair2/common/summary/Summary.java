package com.nodot.background.pair2.common.summary;

import java.util.List;

public interface Summary {
	
	List<?> getMarketData();
	void classificationByProduct();
	void removeDuplicationData();

}
