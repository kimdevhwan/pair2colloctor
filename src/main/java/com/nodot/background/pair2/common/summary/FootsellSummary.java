package com.nodot.background.pair2.common.summary;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nodot.background.pair2.repository.collector.FootsellRepository;
import com.nodot.background.pair2.vo.collector.ExternalMarketVO;
import com.nodot.background.pair2.vo.collector.FootsellVO;
import com.nodot.background.pair2.vo.summary.ProductVO;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class FootsellSummary extends ExternalSummary {
	
	@Autowired
	FootsellRepository footsellRepository;
	
	@Override
	public List<FootsellVO> classifyStocks(String startDate, String endDate, boolean isSearchMode, String searchItem) {
		
		startDate += " 00:00:00";
		endDate += " 23:59:59";
		
		List<FootsellVO> classifiedStocks = new ArrayList<FootsellVO>();
		List<FootsellVO> footsellStocks;
		
		if(isSearchMode) {
			searchItem = searchItem.replaceAll("\\+", "%");
			searchItem = "%" + searchItem + "%";
			footsellStocks = footsellRepository.findSummarizeDataBySearchItem(startDate, endDate, searchItem);
		} else {
			footsellStocks = footsellRepository.findSummarizeDataByDate(startDate, endDate);
		}
		
		for(ProductVO product : super.allProductList()) {
			classifiedStocks.addAll(
				footsellStocks.stream().parallel()
				.filter(f -> f.getSTOCK_NAME().matches(product.getClassifyRegex()))
				.filter(f -> {
					if(product.getFilterRegex() == null)
						return true;
					
					if(f.getSTOCK_NAME().matches(product.getFilterRegex()))
						return false;
					else 
						return true;
				})
				.map(m ->{
					m.setPRODUCT_IDX(product.getIdx());
					return m;
				}).collect(Collectors.toList())
			);
		}
		return classifiedStocks;
	}

	@Override
	public void checkSummarized(List<? extends ExternalMarketVO> classifiedData) {
		classifiedData.forEach( stock -> {
			stock.setIS_SUM(true);
			footsellRepository.save((FootsellVO) stock);
		});
	}
}
