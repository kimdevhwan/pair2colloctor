package com.nodot.background.pair2.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.nodot.background.pair2.vo.collector.ExternalMarketVO;
import com.nodot.background.pair2.vo.collector.FootsellVO;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;
import com.nodot.background.pair2.vo.summary.SummaryVO;

@Component
public class CollectorUtils {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	public String castDate(String rawDate, String rawFormat) throws ParseException {
		String castformat = "yyyy-MM-dd HH:mm:ss";
		
		if(rawDate == null || rawDate.isEmpty())
			return LocalDateTime.now().format(DateTimeFormatter.ofPattern(castformat));
		
		if(rawDate.matches("\\d{2}:\\d{2}")) {
			return LocalDateTime.now().format(DateTimeFormatter.ofPattern(castformat));
		} else if(rawFormat.equals("yy-MM-dd")) {
			LocalDate date = LocalDate.parse(rawDate, DateTimeFormatter.ofPattern(rawFormat));
			return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		} else {
			LocalDateTime date = LocalDateTime.parse(rawDate, DateTimeFormatter.ofPattern(rawFormat));
			return date.format(DateTimeFormatter.ofPattern(castformat));
		}
	}
	
	public synchronized int castPriceWon(String rawPrice) {
		double price = 0.0;
		String tmpPrice = null;
		
		rawPrice = rawPrice.replaceAll(",", ".");
		rawPrice = rawPrice.replaceAll(" ", "");
		
		Pattern p = Pattern.compile("^.+:(?:[a-z|가-힣|,|\\/|\\.]*)(\\d*(?:\\.?\\d*)(?:\\.?\\d*)).*");
		Matcher m = p.matcher(rawPrice);
		
		if(m.matches()) {
			tmpPrice = m.group(1);
			if(tmpPrice.contains(".") && tmpPrice.length() > 5) { //원단위 세자리 구분은 "." 쓴 경우
				tmpPrice = tmpPrice.replace(".", "");
			}
		} else { // 형식에 맡지 않을경우 필터링
			tmpPrice = "0";
		}
		
		if(tmpPrice == null || tmpPrice.isEmpty())
			return 0;
		
		price = Double.parseDouble(tmpPrice);
		
		if(price < 10000) {
			price *= 10000;
		}
		
		return (int) price;
	}
	
	//데이터 시간순 내림차순 정렬
	public List<? extends ExternalMarketVO> sortData(List<? extends ExternalMarketVO> stocks) {
		stocks.sort((s1, s2) -> s1.getREG_DATE().compareTo(s2.getREG_DATE()));
		return stocks;
	}
	
	//전일 날짜 구하기
	public String getLastDate(String format) {
		return LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern(format));
	}
	
	//지정된 시간범위내의 데이터인지 판별
	public boolean isSearchDateRange(String stockDate, String searchStartDate, String searchEndDate) {
		boolean result = false;
		
		LocalDate currentDate = null; 
		LocalDate start = LocalDate.parse(searchStartDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		LocalDate end = LocalDate.parse(searchEndDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		if(stockDate.matches("\\d{2}:\\d{2}")) {
			currentDate = LocalDate.now();
		} else {
			currentDate = LocalDate.parse(stockDate, DateTimeFormatter.ofPattern("yy-MM-dd"));
		}
		
		if((currentDate.equals(start) || currentDate.isAfter(start)) && (currentDate.equals(end) || currentDate.isBefore(end)))
			result = true;
		
		return result;
	}
	
	public String toUrlEncodeForNikemaniaNcf(String itemName) throws UnsupportedEncodingException {
		return URLEncoder.encode(itemName, "EUC-KR");
	}
	
	public SummaryVO toSummarizedData(ExternalMarketVO externalData) {
		SummaryVO sumData = new SummaryVO();
		
		if(externalData instanceof FootsellVO) {
			FootsellVO stock = (FootsellVO) externalData;
			sumData.setSELLER_NICK(stock.getUSER_NICK());
			sumData.setREG_DATE(stock.getREG_DATE());
			sumData.setSIZE(stock.getSTOCK_SIZE());
			sumData.setPRICE(stock.getSTOCK_PRICE());
			sumData.setVIEW_COUNT(stock.getVIEW_COUNT());
			sumData.setDATA_SOURCE("FOOTSELL");
			sumData.setEXTERNAL_IDX(stock.getIDX());
			sumData.setPRODUCT_IDX(stock.getPRODUCT_IDX());
		}
		
		if(externalData instanceof NikemaniaNcfVO) {
			NikemaniaNcfVO stock = (NikemaniaNcfVO) externalData;
			sumData.setSELLER_NICK(stock.getUSER_NICK());
			sumData.setREG_DATE(stock.getREG_DATE());
			sumData.setSIZE(stock.getSTOCK_SIZE());
			sumData.setPRICE(stock.getSTOCK_PRICE());
			sumData.setVIEW_COUNT(stock.getVIEW_COUNT());
			sumData.setDATA_SOURCE("NIKEMANIA_NCF");
			sumData.setEXTERNAL_IDX(stock.getIDX());
			sumData.setPRODUCT_IDX(stock.getPRODUCT_IDX());
		}
		
		return sumData;
	}
	
	public boolean isNumeric(String number) {
		try {
			Double.parseDouble(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public boolean isValidshoesSize(String s) {
		
		if(!isNumeric(s))
			return false;
		
		int size = Integer.parseInt(s);
		
		if(size < 230 || size > 300)
			return false;
		else
			return true;
	}

}
