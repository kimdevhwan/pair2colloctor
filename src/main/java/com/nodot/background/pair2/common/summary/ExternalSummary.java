package com.nodot.background.pair2.common.summary;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.repository.summary.ProductRepository;
import com.nodot.background.pair2.repository.summary.SummaryRepository;
import com.nodot.background.pair2.vo.collector.ExternalMarketVO;
import com.nodot.background.pair2.vo.summary.ProductVO;
import com.nodot.background.pair2.vo.summary.SummaryVO;

@Component
public abstract class ExternalSummary {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	SummaryRepository summaryRepository;
	
	@Autowired
	CollectorUtils collectorUtils;
	
	public List<ProductVO> allProductList() {
		return productRepository.findAll();
	}
	
	private List<SummaryVO> summarize(List<? extends ExternalMarketVO> classifiedStocks) {
		List<SummaryVO> summarizedStocks = classifiedStocks.stream().parallel()
				.map(m -> collectorUtils.toSummarizedData(m))
				.collect(Collectors.toList());
		return summarizedStocks;
	}
	
	public void saveSummary(List<? extends ExternalMarketVO> classifiedStocks) {
		summaryRepository.saveAll(summarize(classifiedStocks));
	}
	
	public abstract List<? extends ExternalMarketVO> classifyStocks(String startDate, String endDate, boolean isSearchMode, String searchItemName);
	public abstract void checkSummarized(List<? extends ExternalMarketVO> classifiedData);
}
