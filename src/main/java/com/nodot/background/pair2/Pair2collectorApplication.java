package com.nodot.background.pair2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pair2collectorApplication {
	
	public static void main(String[] args) {
		/**
		 * Running Types
		 * * nikemaniancf
		 * * footsell
		 * * test
		 */
		SpringApplication.run(Pair2collectorApplication.class, args[0]);
	}
	
}