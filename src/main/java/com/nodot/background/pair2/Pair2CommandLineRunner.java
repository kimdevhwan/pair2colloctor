package com.nodot.background.pair2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.service.FootsellService;
import com.nodot.background.pair2.service.NikemaniaNcfService;
import com.nodot.background.pair2.service.SummaryService;

@Component
public class Pair2CommandLineRunner implements CommandLineRunner {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private NikemaniaNcfService nikemaniaNcfService;
	
	@Autowired
	private FootsellService footsellService;
	
	@Autowired
	private SummaryService summaryService;
	
	@Autowired
	private CollectorUtils collectorUtils;

	@Override
	public void run(String... args) throws Exception {
		
		boolean isSearchMode = false;
		String searchItemName = "";
		String startDate = collectorUtils.getLastDate("yyyy-MM-dd");
		String endDate = collectorUtils.getLastDate("yyyy-MM-dd");
		
		if(args[0].equals("nikemaniancf")) {	//나매  프로세스
			nikemaniaNcfService.initialize(isSearchMode, searchItemName);
			nikemaniaNcfService.createCookies();
			nikemaniaNcfService.collectData(startDate, endDate);
		} else if(args[0].equals("footsell")) {	//풋셀 프로세스
			footsellService.initialize(isSearchMode, searchItemName);
			footsellService.collectData(startDate, endDate);
		} else if(args[0].equals("test")) {	//풋셀 프로세스 {
			logger.info("============== Start Unit Test mode ==============");
		} else {
			throw new IllegalArgumentException("Wrong Input Runner Type Argument "+args[0]);
		}
		
		summaryService.initialize(isSearchMode, searchItemName);
		summaryService.summarize(startDate, endDate);
		
	}

}
