package com.nodot.background.pair2.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.NaverCafeUtils;
import com.nodot.background.pair2.common.summary.NikemainiaNcfSummary;
import com.nodot.background.pair2.repository.collector.NikemaniaNcfRepository;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;

@Service
public class NikemaniaNcfService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CollectorUtils commonUtils;

	@Autowired
	private NaverCafeUtils naverUtils;
	
	@Autowired
	private CrawlerForNikemaniaNcf crawlerNikemaniaNcf;
	
	@Autowired
	private NikemaniaNcfRepository nikemaniaNCfRepository;
	
	private boolean isSearchMode = false;
	private String searchItemName;
	
	public void initialize(boolean isSearchMode, String searchItemName) throws UnsupportedEncodingException {
		this.isSearchMode = isSearchMode;
		this.searchItemName = commonUtils.toUrlEncodeForNikemaniaNcf(searchItemName);
	}
	
	public void createCookies() throws InterruptedException {
		logger.info("============== 네이버 수동 로그인  ==============");
		naverUtils.createCookiesByBrowser();
	}
	
	@SuppressWarnings("unchecked")
	public void collectData(String startDate, String endDate) {
		Optional.ofNullable(naverUtils.getCookie())
			.filter(m -> m.get("PM_CK_rcode") != null)
			.ifPresent( cookies -> {
				cookies.forEach((k,v) -> {
					logger.info("Cookie -> {} : {}",k,v);
				});
				
				crawlerNikemaniaNcf.initialize(startDate, endDate, isSearchMode, searchItemName);
				logger.info("============== Start getting stocks Nikemania NCF ==============");
				logger.info("============== Collecting date : {} - {}", startDate, endDate);
				
				try {
					List<NikemaniaNcfVO> stocks = crawlerNikemaniaNcf.getStocks(cookies);
					nikemaniaNCfRepository.saveAll((List<NikemaniaNcfVO>) commonUtils.sortData(stocks));
					logger.info("============== Collected stocks in Nikemania NCF : {}", stocks.size());
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
	}

}
