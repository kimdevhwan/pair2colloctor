package com.nodot.background.pair2.service;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.summary.FootsellSummary;
import com.nodot.background.pair2.common.summary.NikemainiaNcfSummary;
import com.nodot.background.pair2.vo.collector.ExternalMarketVO;
import com.nodot.background.pair2.vo.collector.FootsellVO;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;

@Service
public class SummaryService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private NikemainiaNcfSummary nikemaniaNcfSummary;
	
	@Autowired
	private FootsellSummary footsellSummary;
	
	@Autowired
	private CollectorUtils collectorUtils;
	
	private boolean isSearchMode = false;
	private String searchItemName;
	
	public void initialize(boolean isSearchMode, String searchItemName) {
		this.isSearchMode = isSearchMode;
		this.searchItemName = searchItemName;
	}
	
	public void summarize(String startDate, String endDate) {
		logger.info("============== Start Summarize date : {} - {}", startDate, endDate);
		List<ExternalMarketVO> classifiedStocks = new LinkedList<ExternalMarketVO>();
		
		List<NikemaniaNcfVO> nikemaniaNcfStocks = nikemaniaNcfSummary.classifyStocks(startDate, endDate, isSearchMode, searchItemName);
		List<FootsellVO> footsellStocks = footsellSummary.classifyStocks(startDate, endDate, isSearchMode, searchItemName);
		
		classifiedStocks.addAll(nikemaniaNcfStocks);
		classifiedStocks.addAll(footsellStocks);
		
		if(classifiedStocks.size() > 0) {
			nikemaniaNcfSummary.saveSummary(collectorUtils.sortData(classifiedStocks));
			nikemaniaNcfSummary.checkSummarized(nikemaniaNcfStocks);
			footsellSummary.checkSummarized(footsellStocks);
		}
		
		logger.info("============== Summarized stocks in NikeMania NCF : {}", nikemaniaNcfStocks.size());
		logger.info("============== Summarized stocks in Footsell : {}", footsellStocks.size());
		
	}
}
