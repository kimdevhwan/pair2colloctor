package com.nodot.background.pair2.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.summary.FootsellSummary;
import com.nodot.background.pair2.repository.collector.FootsellRepository;
import com.nodot.background.pair2.vo.collector.FootsellVO;

@Service
public class FootsellService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CollectorUtils commonUtils;
	
	@Autowired
	private CrawlerForFootsell crawlerFootsell;
	
	@Autowired
	private FootsellRepository footsellRepository;
	
	private boolean isSearchMode = false;
	private String searchItemName;
	
	public void initialize(boolean isSearchMode, String searchItemName) {
		this.isSearchMode = isSearchMode;
		this.searchItemName = searchItemName;
	}
	
	@SuppressWarnings("unchecked")
	public void collectData(String startDate, String endDate) throws IOException {
		logger.info("============== Start getting stocks Footsell ==============");
		logger.info("============== Collecting date : {} - {}", startDate, endDate);
		crawlerFootsell.initialize(startDate, endDate, isSearchMode, searchItemName);
		List<FootsellVO> stocks = crawlerFootsell.getStocks();
		logger.info("============== Collected stocks in Footsell : {}", stocks.size());
		footsellRepository.saveAll((List<FootsellVO>) commonUtils.sortData(stocks));
	}
	
}
