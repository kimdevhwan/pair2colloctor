package com.nodot.background.pair2.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.gargoylesoftware.htmlunit.WebClient;
import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.NaverCafeUtils;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;

@Service
public class CrawlerForNikemaniaNCafe {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String ID = "";
	private final String PW = "";
	
	private NaverCafeUtils naverUtil;
	private CollectorUtils commonUtil;
	
	private String nikemaniaNCafeUrl = "https://cafe.naver.com/ArticleList.nhn?search.clubid=10625158";
//	private String nikemaniaNCafeUrl = "https://cafe.naver.com/ArticleSearchList.nhn?search.clubid=10625158&search.menuid=373&search.media=0&search.searchdate=2018-10-192018-10-21&search.include=&userDisplay=50&search.exclude=&search.option=0&search.sortBy=date&search.searchBy=0&search.includeAll=&search.query=&search.viewtype=title&search.page=";
	
	private String[] menuIds = {
			"&search.menuid=369"//조던 MAN //3-8 5
			,"&search.menuid=371"//나이키 신발 //12-29 17
			,"&search.menuid=448"//아디다스 신발 //6-16 10
			,"&search.menuid=373"//타브랜드 신발 //5-12 7
	};
	private String boardType = "&search.boardtype=L";
	private String boardPerPage = "&userDisplay=50";
	private String boradPage = "&search.page=";
	
	private String tagStocksArea = "form[name=ArticleList] > table.board-box > tbody > tr[align=center]";
	private String tagRegDate = "td.view-count.m-tcol-c";
	private String tagSellingStatus = "span.head";
	private String tagStock = "span.aaa > a.m-tcol-c";
	private String tagStockUrl = "abs:href";
	private String tagSellerNick = "div.pers_nick_area > table > tbody > tr > td.p-nick > a";
	private String tagStockDetaillArea = "div.inbox";
	private String tagStockDetailRegDate = "div.tit-box > div.fr";
	private String tagStockDetail = "#tbody > div";
	
	private WebClient webClient;
	
	private Map<String, String> cookies;
	
	private List<NikemaniaNcfVO> data;
	
	private int checkPageOffset;
	private String lastDate;
	
	public CrawlerForNikemaniaNCafe() {
		naverUtil = new NaverCafeUtils();
		commonUtil = new CollectorUtils();
		data = new LinkedList<NikemaniaNcfVO>();
		webClient = naverUtil.initWebClient();
		lastDate = commonUtil.getLastDate("yyyy.MM.dd.");
		logger.info(lastDate);
	}
	
	public WebClient login() {
		long waitTimeMillis = 1000;
		int tryLoginCount = 1;
		
		try {
			webClient = naverUtil.login(ID, PW, webClient);
			while(!naverUtil.isLogined(webClient)) {
				waitTimeMillis += 500;
				tryLoginCount++;
				
				if(tryLoginCount > 20) {
					throw new TimeoutException("Naver Login fail......");
				}
				
				webClient = naverUtil.initWebClient(waitTimeMillis);
				webClient = naverUtil.login(ID, PW, webClient);
			}
			logger.info("Naver Login Sucess. Try to it {} times....",tryLoginCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webClient;
	}
	
	public List<NikemaniaNcfVO> getStocks() throws IOException {
		
		cookies = naverUtil.getCookie(this.webClient);
		
		//사이즈 제품명 분리 정규식 패턴
		Pattern sizeNamePattern = Pattern.compile("^[\\[|\\(].*?(\\d+).*?[\\]|/)]\\s*(.+)$");
		
		for (String menuId : menuIds) {
			for(int pageNum=1; pageNum<30; pageNum++) {
				checkPageOffset = 0;
				StringBuilder url = new StringBuilder();
				url.append(nikemaniaNCafeUrl);
				url.append(menuId);
				url.append(boardType);
				url.append(boardPerPage);
				url.append(boradPage);
				url.append(pageNum);
				
//				logger.info("{}, {} page parsing.......",menuId,pageNum);
				
				logger.info(url.toString());
				
				Document doc = Jsoup.connect(url.toString()).cookies(cookies).get();
				//닉네임, 판매중 여부, 제품명, 제품 URL
				if(doc != null) {
					Elements stockList = doc.select(tagStocksArea);
					stockList.stream()
					.filter(c -> c.select(tagRegDate).first().text().equals(lastDate))	//전일 날짜 데이터 필터링
					.filter(c -> c.select(tagSellingStatus).text().equals("[판매중]"))	//판매중 제품 필터링
					.filter(c -> !c.select(tagStock).text().matches(".+[,|\\/|\\&|\\|].+"))	//다중판매 필터링
					.forEach(element -> {
						NikemaniaNcfVO nmData = new NikemaniaNcfVO();
						
						//사이즈, 제품명 추출
						String sizeName = element.select(tagStock).text();
						Matcher m = sizeNamePattern.matcher(sizeName);
						if(m.matches()) { //사이즈, 제품명 분리
							nmData.setSTOCK_SIZE(m.group(1));
							String name = m.group(2);
							if(name.length() > 60)
								name = name.substring(0, 59);
							nmData.setSTOCK_NAME(name);
						}
						
						nmData.setUSER_NICK(element.select(tagSellerNick).text());
						nmData.setSTOCK_URL(element.select(tagStock).attr(tagStockUrl));
						try {
							this.getStockDatePriceStatus(nmData, cookies);
							if(this.isVaildStockData(nmData)) {
								data.add(nmData);
								checkPageOffset++;
							}
						} catch (Exception e) {
							e.printStackTrace();
//							logger.error(e.getMessage(),e.getCause());
						}
					});
					
					if(checkPageOffset < 1)	//전일 날짜 데이터가 없으면 종료 offset
						break;
				}
			}
		}
		return data;
	}
	
	/*
	 * 게시물 상세보기에서 매물 등록일자와 가격 정보을 얻는다.
	 */
	public void getStockDatePriceStatus(NikemaniaNcfVO data, Map<String, String> cookies) throws Exception {
		Document doc = Jsoup.connect(data.getSTOCK_URL()).cookies(cookies).get();
		Elements contents = doc.select(tagStockDetaillArea);
		
		data.setREG_DATE(commonUtil.castDate(contents.select(tagStockDetailRegDate).text(), "yyyy.MM.dd. HH:mm"));
		Elements stockInfo = contents.select(tagStockDetail);
		Elements stockDesc = stockInfo.select("p");
		
		Optional<Element> price = stockDesc.stream().filter(c->c.text().matches(".?희망가격.+?:.+")).findFirst();
		if(price.isPresent()) {
//			logger.info("{}",price.get().text());
			if(price.get().getElementsByTag("STRIKE").isEmpty()) {
//				logger.info("price : {}", commonUtil.castPriceWon(price.get().text()));
				data.setSTOCK_PRICE(commonUtil.castPriceWon(price.get().text()));
			}
		}
		
		Optional<Element> status = stockDesc.stream().filter(c->c.text().matches(".?제품상태.+?:.+")).findFirst();
		if(status.isPresent()) {
			String statusData = status.get().text().replaceAll(" ", "");
			
			boolean isNew = false;
			boolean isUesd = statusData.matches("^.+:.*((중고)|(착용)|(실착)|(신었))+.*$");
			if(isUesd == true)
				isNew = false;
			else
				isNew = statusData.matches("^.+:.*((새제품)|(새상품)|(신품)|(new))+.*$");
			
			data.setIS_NEW(isNew);
		}
	}
	
	private boolean isVaildStockData(NikemaniaNcfVO data) {
		if (data != null &&
			data.getUSER_NICK() != null && !data.getUSER_NICK().isEmpty() &&
			data.getSTOCK_NAME() != null && !data.getSTOCK_NAME().isEmpty() &&
			data.getSTOCK_SIZE() != null && !data.getSTOCK_SIZE().isEmpty() &&
			data.getSTOCK_PRICE() > 0)
			return true;
		
		return false;
	}
	
}
