package com.nodot.background.pair2.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.WebClient;
import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.common.NaverCafeUtils;
import com.nodot.background.pair2.vo.collector.NikemaniaNcfVO;

@Component
public class CrawlerForNikemaniaNcf {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String ID = null;
	private final String PW = null;
	
	@Autowired
	private NaverCafeUtils naverUtil;
	
	@Autowired
	private CollectorUtils commonUtil;
	
	private String nikemaniaNCafeSearchUrl = "https://cafe.naver.com/ArticleSearchList.nhn?search.clubid=10625158";
	
	private String[] menuIds = {
			"&search.menuid=369"//조던 MAN //3-8 5
			,"&search.menuid=408"//조던 GS //3-8 5
			,"&search.menuid=371"//나이키 신발 //12-29 17
			,"&search.menuid=448"//아디다스 신발 //6-16 10
			,"&search.menuid=373"//타브랜드 신발 //5-12 7
	};
	
	private boolean isSearchMode = false;
	
	private String boardType = "&search.boardtype=L";
	private String boardPerPage = "&userDisplay=50";
	private String boradPage = "&search.page=";
	private String searchDate = "&search.searchdate=";
	private String searchUrl = "&search.query=";
	
	private String tagStocksArea = "form[name=ArticleList] > table.board-box > tbody > tr[align=center]";
	private String tagSellingStatus = "span.head";
	private String tagStockTitle = "span.aaa > a.m-tcol-c";
	private String tagStockUrl = "abs:href";
	private String tagSellerNick = "div.pers_nick_area > table > tbody > tr > td.p-nick > a";
	private String tagViewCount = "td.view-count.m-tcol-c";
	private String tagStockDetaillArea = "div.inbox";
	private String tagStockDetailRegDate = "div.tit-box > div.fr";
	private String tagStockDetail = "#tbody > div";
	
	private String regexBlindFilter = ".*((?:일괄)|(?:(?i)ps)|(?:(?i)bp)|(?:(?i)bg)|(?:(?i)td)).*";
	
	private WebClient webClient;
	
	private List<NikemaniaNcfVO> data;
	
	private int checkPageOffset;
	
	public void initialize(String startDate, String endDate) {
		data = new LinkedList<NikemaniaNcfVO>();
		webClient = naverUtil.initWebClient();
		searchDate += (startDate + endDate);
	}
	
	public void initialize(String startDate, String endDate, boolean isSearchMode, String itemName) {
		data = new LinkedList<NikemaniaNcfVO>();
		webClient = naverUtil.initWebClient();
		searchDate += (startDate + endDate);
		this.isSearchMode = isSearchMode;
		this.searchUrl += itemName;
	}
	
	public WebClient login() {
		long waitTimeMillis = 1000;
		int tryLoginCount = 1;
		
		try {
			webClient = naverUtil.login(ID, PW, webClient);
			while(!naverUtil.isLogined(webClient)) {
				waitTimeMillis += 500;
				tryLoginCount++;
				
				if(tryLoginCount > 20) {
					throw new TimeoutException("Naver Login fail......");
				}
				
				webClient = naverUtil.initWebClient(waitTimeMillis);
				webClient = naverUtil.login(ID, PW, webClient);
			}
			logger.info("Naver Login Sucess. Try to it {} times....",tryLoginCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webClient;
	}
	
	public List<NikemaniaNcfVO> getStocks(Map<String, String> cookies) throws IOException {
		
		//사이즈 제품명 분리 정규식 패턴
		Pattern sizeNamePattern = Pattern.compile("^[\\[|\\(].*?(\\d+).*?[\\]|/)]\\s*(.+)$");
		
		for (String menuId : menuIds) {
			for(int pageNum=1; pageNum<100; pageNum++) {
				checkPageOffset = 0;
				StringBuilder url = new StringBuilder();
				url.append(nikemaniaNCafeSearchUrl);
				url.append(menuId);
				url.append(boardType);
				url.append(boardPerPage);
				url.append(searchDate);
				url.append(boradPage);
				url.append(pageNum);
				
				if(isSearchMode)
					url.append(searchUrl);
				
				logger.info(url.toString());
				
				Document doc = Jsoup.connect(url.toString()).cookies(cookies).get();
				if(doc != null) {
					Elements stockList = doc.select(tagStocksArea);
					stockList.stream()
					.filter(c -> !c.select(tagStockTitle).text().matches(".+[,|\\/|\\&|\\|].+"))	//다중판매 필터링
					.filter(c -> !c.select(tagStockTitle).text().matches(regexBlindFilter))	//다중판매 필터링
					.forEach( element -> {
						NikemaniaNcfVO nmData = new NikemaniaNcfVO();
						
						if(element.select(tagSellingStatus).text().equals("[판매완료]") ||
								element.select(tagStockTitle).text().matches(".*판매완료.*")) {
							nmData.setIS_SOLDOUT(true);
						}
						
						//사이즈, 제품명 추출
						String sizeName = element.select(tagStockTitle).text();
						Matcher m = sizeNamePattern.matcher(sizeName);
						if(m.matches()) { //사이즈, 제품명 분리
							
							if(!commonUtil.isValidshoesSize(m.group(1)))
								return;
							
							nmData.setSTOCK_SIZE(m.group(1));
							String name = m.group(2);
							if(name.length() > 60)	name = name.substring(0, 59);
							nmData.setSTOCK_NAME(name);
						}
						nmData.setUSER_NICK(element.select(tagSellerNick).text());
						nmData.setSTOCK_URL(element.select(tagStockTitle).attr(tagStockUrl));
						nmData.setVIEW_COUNT(Integer.parseInt(element.select(tagViewCount).get(1).text()));
						
						try {
							this.getStockDatePriceStatus(nmData, cookies);
							if(this.isVaildStockData(nmData)) {
								data.add(nmData);
								checkPageOffset++;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
					
					if(checkPageOffset < 1)	//한페이지에 대상 날짜 데이터가 없으면 종료 offset
						break;
				}
			}
		}
		return data;
	}
	
	/*
	 * 게시물 상세보기에서 매물 등록일자와 가격 정보을 얻는다.
	 */
	public void getStockDatePriceStatus(NikemaniaNcfVO data, Map<String, String> cookies) throws Exception {
		Document doc = Jsoup.connect(data.getSTOCK_URL()).cookies(cookies).get();
		Elements contents = doc.select(tagStockDetaillArea);
		
		data.setREG_DATE(commonUtil.castDate(contents.select(tagStockDetailRegDate).text(), "yyyy.MM.dd. HH:mm"));
		Elements stockInfo = contents.select(tagStockDetail);
		Elements stockDesc = stockInfo.select("p");
		
		Optional<Element> price = stockDesc.stream().filter(c->c.text().matches(".?희망가격.+?:.+")).findFirst();
		if(price.isPresent()) {
			if(price.get().getElementsByTag("STRIKE").isEmpty()) {
				data.setSTOCK_PRICE(commonUtil.castPriceWon(price.get().text()));
			}
		}
		
		//판매가격이 0원 일때 판매완료 된 것으로 판단
		if(data.getSTOCK_PRICE() == 0) {
			data.setIS_SOLDOUT(true);
		}
		
		Optional<Element> status = stockDesc.stream().filter(c->c.text().matches(".?제품상태.+?:.+")).findFirst();
		if(status.isPresent()) {
			String statusData = status.get().text().replaceAll(" ", "");
			
			boolean isNew = false;
			boolean isUesd = statusData.matches("^.+:.*((중고)|(착용)|(실착)|(신었))+.*$");
			if(isUesd == true)
				isNew = false;
			else
				isNew = statusData.matches("^.+:.*((새제품)|(새상품)|(신품)|(new))+.*$");
			
			data.setIS_NEW(isNew);
		}
	}
	
	private boolean isVaildStockData(NikemaniaNcfVO data) {
		if (data != null && data.getUSER_NICK() != null && !data.getUSER_NICK().isEmpty()
				&& data.getSTOCK_NAME() != null && !data.getSTOCK_NAME().isEmpty()) {
			return true;
		}
		
		return false;
	}
	
}
