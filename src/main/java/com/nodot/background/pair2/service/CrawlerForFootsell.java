package com.nodot.background.pair2.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.nodot.background.pair2.common.CollectorUtils;
import com.nodot.background.pair2.vo.collector.FootsellVO;

@Component
public class CrawlerForFootsell {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CollectorUtils commonUtils;
	
	private boolean isSearchMode = false;
	private String startDate = null;
	private String endDate = null;
	
	private String footsellBaseUrl = "https://footsell.com/g2/bbs/board.php?bo_table=m51&product_status=1&page=";
	private String searchUrl = "&sfl=wr_subject&sop=and&stx=";
	
	private List<FootsellVO> data;
	
	private String tagStock = "div#list_table > div.list_table_row";
	private String tagUserNick = "div.list_market_name";
	private String tagSize = "span.list_market_size";
	private String tagPrice = "div.list_market_price";
	private String tagName = "a.list_subject_a";
	private String tagCrawlingDate = "span.list_table_dates";
	private String tabIsNew = "span.list_market_used";
	private String tagSizeWonUnit = "span.list_market_size_mm";
	private String tagViewCount = "div.float_right.list_table_col_hit";
	
//	private String regexBlindFilter = ".*((?:블라인드)|(?:교환)|(?:일괄)|(?:(?i)gs)|(?:(?i)ps)|(?:(?i)bp)|(?:(?i)td)).*";
	private String regexBlindFilter = ".*((?:블라인드)|(?:교환)|(?:일괄)|(?:(?i)ps)|(?:(?i)bp)|(?:(?i)td)).*";
	
	private int checkPageOffset;
	
	public CrawlerForFootsell() {
		data = new LinkedList<FootsellVO>();
	}
	
	public void initialize(String startDate, String endDate, boolean isSearchMode, String searchItemName) {
		this.isSearchMode = isSearchMode;
		this.searchUrl += searchItemName;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public List<FootsellVO> getStocks() throws IOException{
		for(int pageNum=1; pageNum<100; pageNum++) {
			checkPageOffset = 0;
			
			StringBuilder url = new StringBuilder();
			url.append(footsellBaseUrl);
			url.append(pageNum);
			if(isSearchMode)
				url.append(searchUrl);
			
			logger.info("{} parsing.......",url.toString());
			
			Document doc = Jsoup.connect(url.toString()).get();
			if(doc != null) {
				
				Elements stockList = doc.select(tagStock);
				stockList.select(tagSizeWonUnit).remove();	//원화,mm 태그 제거
				
				stockList.stream()
					.filter(f -> commonUtils.isSearchDateRange(f.select(tagCrawlingDate).text(), startDate, endDate))
					.filter(f -> f.select(tagSize).text().length() < 4) //여러 사이즈 일괄 등록 필터링
					.filter(f -> f.select(tagPrice).text().length() > 4) //1만원 이하 가격 필터링
					.filter(f -> !f.select(tagName).text().matches(regexBlindFilter))
					.forEach(stock -> {
						FootsellVO fsData = new FootsellVO();
						
						try {
							
							String size = stock.select(tagSize).text();
							
							if(!commonUtils.isValidshoesSize(size))
								return;
							
							fsData.setUSER_NICK(stock.select(tagUserNick).text());
							fsData.setSTOCK_NAME(stock.select(tagName).text());
							fsData.setSTOCK_SIZE(size);
							fsData.setSTOCK_PRICE(Integer.parseInt(stock.select(tagPrice).text().replaceAll(",", "")));
							fsData.setVIEW_COUNT(Integer.parseInt(stock.select(tagViewCount).text()));
							fsData.setSTOCK_URL(stock.select(tagName).attr("abs:href"));
							fsData.setREG_DATE(commonUtils.castDate(stock.select(tagCrawlingDate).text(), "yy-MM-dd"));
							fsData.setIS_NEW(stock.select(tabIsNew).text().equals("새제품") ? true : false);
							fsData.setIS_SOLDOUT(stock.select(tagUserNick).text().equals("거래완료") ? true : false);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						if(this.isVaildStockData(fsData)) {
							data.add(fsData);
							checkPageOffset++;
						}
					});
				
				if(checkPageOffset < 1)	//전일 날짜 데이터가 없으면 종료
					break;
			}
		}
		return data;
	}
	
	private boolean isVaildStockData(FootsellVO data) {
		if (data != null &&
			data.getUSER_NICK() != null && !data.getUSER_NICK().isEmpty() &&
			data.getSTOCK_NAME() != null && !data.getSTOCK_NAME().isEmpty()
			)
			return true;
		
		return false;
	}

}
